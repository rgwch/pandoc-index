const fs = require('fs')
let current = ""
let words = {}

/**
 * retrieve the pure text content of an Element and all enclosed elements
 */
function textContent(elem) {
  if (elem === Object(elem) && elem.t === 'MetaString') return elem.c;

  var result = [];
  var go = function (key, val) {
    if (key === 'Str') result.push(val);
    else if (key === 'Code') result.push(val[1]);
    else if (key === 'Math') result.push(val[1]);
    else if (key === 'LineBreak') result.push(' ');
    else if (key === 'Space') result.push(' ');
  };
  traverse(elem, go);
  return result.join('');
}

/**
 * Parse an element for headers index-entries
 * if the type is headers: remember to associate it with index terms
 * if the type is span: check for index-entries and if found, associate them with the previous heading
 * @param {*} type 
 * @param {*} value 
 */
function parse(type, value) {
  if (type == "Header") {
    if (Array.isArray(value)) {
      const _level = value[0]
      const _attribs = value[1]
      const contents = value[2]
      if (Array.isArray(_attribs)) {
        const _id = _attribs[0];
        current = { text: textContent(value), id: _id };
      }
    }
  } else if (type === 'Span') {
    if (Array.isArray(value)) {
      const attribs = value[0]
      const contents = value[1]
      if (Array.isArray(attribs)) {
        const _id = attribs[0];
        const _classes = attribs[1];
        if (Array.isArray(contents) && contents.length > 0) {
          const desc = textContent(contents)
          if (_classes.some(el => el === "index")) {
            const foundIndex = desc

            if (Array.isArray(words[foundIndex])) {
              words[foundIndex].push(current);
            } else {
              words[foundIndex] = [current]
            }
          }
        }
      }
    }
  }
}

/**
 * Traverse an object ree
 * @param {*} obj 
 * @param {*} func 
 */
function traverse(obj, func) {
  if (Array.isArray(obj)) {
    obj.forEach(item => {
      if (item === Object(item) && item.t) {
        func(item.t, item.c || [])
        traverse(item, func)
      } else {
        traverse(item, func)
      }
    })
  } else if (obj === Object(obj)) {
    Object.keys(obj).forEach(key => {
      traverse(obj[key], func)
    })
  }
}

/**
 * Main entry point: Create an index from a pandoc AST in json form
 * @param {*} infile file to read (must be json)
 * @param {*} outfile file to write (will be markdown)
 */
function createIndex(infile, outfile) {
  fs.readFile(infile, (err, json) => {
    if (err) {
      throw (err)
    }
    var data = JSON.parse(json)
    traverse(data.blocks, parse)
    const list = []
    Object.keys(words).forEach(key => {
      list.push(key)
    })
    const ordered = list.sort((a, b) => {
      let al = a.toLocaleLowerCase();
      let bl = b.toLocaleLowerCase();
      if (al < bl) {
        return -1
      } else if (al > bl) {
        return 1
      } else {
        return 0
      }
    })
    const outmd = fs.createWriteStream(outfile)
    ordered.forEach(elem => {
      outmd.write("* " + elem + ": \t")
      words[elem].forEach(word => {
        outmd.write("[" + word.text + "](#" + word.id + "),")
      })
      outmd.write("\n")
    })
    
    // outmd.close();
    //process.stdout.write(JSON.stringify(data));

  })
}

module.exports = {
  createIndex: createIndex
}
