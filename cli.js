#! /usr/bin/env node

const [,, ...args]= process.argv

const indexer=require('./index.js')
if((args.length!=1) || args[0] =="?" || args[0] == "--help"){
    require('fs').createReadStream(__dirname + '/usage.txt').pipe(process.stdout);
    return;
}
console.log(`creating index ${args[0]}.json to ${args[0]}.md`)
indexer.createIndex(args[0]+".json",args[0]+".md");
