## About

Create an index for markdown/pandoc books.

I wrote a technical book in markdown and needed a possibility to create an index of important terms.
I did not find something usable for me (did not want to install the whole LaTeX suite just to create an index). So I wrote this rather quick and dirty tool. There are probably better ways. YMMV.

## Install

npm install -g pandoc-index

## Usage


* Mark up all interesting terms with the index class, such as: *This is an interesting [term]{.index} to add to the index*.

* Create a JSON representation of Pandoc's AST tree: `pandoc -o myindex.json part1.md part2.md part3.md`. This will output a file myindex.json.

* Create the index: `pandoc-index myindex`. This will read myindex.json and output myindex.md

* Create the book: `pandoc -o mybook.epub -t epub3 part1.md part2.md part3.md myindex.md`.

## Note

Some of the code is adapted from [pandoc-filter](https://www.npmjs.com/package/pandoc-filter). pandoc-index is, however, not a pandoc filter since we don't change the original text at all.


## Credits

Thanks to [John MacFarlane](https://github.com/jgm) for pandoc and to [Mike Henderson](https://github.com/mvhenderson) für pandoc-filter.

## License

Apache 2.0
